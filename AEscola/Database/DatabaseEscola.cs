﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEscola.Database
{
    class DatabaseEscola
    {
        public void Inserir(Database.Entyti.tb_turma turma)
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            db.tb_turma.Add(turma);

            db.SaveChanges();
        }

        public List<Database.Entyti.tb_turma> Consultar(string curso, int MaxAlunos)
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            List<Entyti.tb_turma> turmas = db.tb_turma.Where(t => t.nm_curso == curso && t.qt_max_alunos >= MaxAlunos).ToList();
            return turmas;
        }

        public List<Database.Entyti.tb_turma> Listar()
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            List<Entyti.tb_turma> turmas = db.tb_turma.ToList();
            return turmas;
        }

        public List<Database.Entyti.tb_turma> Consultar2(int MaxAlunos)
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            List<Entyti.tb_turma> turmas = db.tb_turma.Where(t => t.qt_max_alunos >= MaxAlunos).ToList();
            return turmas;
        }

        public List<Database.Entyti.tb_turma> Consultar3(string curso)
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            List<Entyti.tb_turma> turmas = db.tb_turma.Where(t => t.nm_curso == curso).ToList();
            return turmas;
        }

        public List<Database.Entyti.tb_turma> Consultar4(string turma)
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            List<Entyti.tb_turma> turmas = db.tb_turma.Where(t => t.nm_turma == turma).ToList();
            return turmas;
        }

        public void Alterar(Database.Entyti.tb_turma turmas)
        {
            Entyti.schooldbEntities o = new Entyti.schooldbEntities();
            Entyti.tb_turma alterar = o.tb_turma.First(a => a.id_turma == turmas.id_turma);
            alterar.nm_curso = turmas.nm_curso;
            alterar.nm_turma = turmas.nm_turma;
            alterar.qt_max_alunos = turmas.qt_max_alunos;

            o.SaveChanges();

        }

        public void Remover(Database.Entyti.tb_turma turma)
        {
            Entyti.schooldbEntities o = new Entyti.schooldbEntities();

            Entyti.tb_turma remove = o.tb_turma.First(a => a.id_turma == turma.id_turma);
            o.tb_turma.Remove(remove);

            o.SaveChanges();
        }
        public void Remover2(Database.Entyti.tb_turma turma)
        {
            Entyti.schooldbEntities o = new Entyti.schooldbEntities();

            Entyti.tb_turma remove = o.tb_turma.First(a => a.nm_turma == turma.nm_turma);
            o.tb_turma.Remove(remove);

            o.SaveChanges();
        }
        public void Alterar2(Database.Entyti.tb_turma turmas)
        {
            Entyti.schooldbEntities o = new Entyti.schooldbEntities();
            Entyti.tb_turma alterar = o.tb_turma.First(a => a.nm_turma == turmas.nm_turma);
            alterar.nm_curso = turmas.nm_curso;
            alterar.qt_max_alunos = turmas.qt_max_alunos;

            o.SaveChanges();
        }

        public void InserirAluno(Database.Entyti.tb_aluno aluno)
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            db.tb_aluno.Add(aluno);

            db.SaveChanges();
        }
        public List<Database.Entyti.tb_aluno> ConsultarAlunos(string nome)
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            List<Entyti.tb_aluno> turmas = db.tb_aluno.Where(t => t.nm_aluno == nome).ToList();
            return turmas;
        }
        public List<Database.Entyti.tb_aluno> ConsultarAluno()
        {
            Entyti.schooldbEntities db = new Entyti.schooldbEntities();
            List<Entyti.tb_aluno> aluno = db.tb_aluno.OrderBy(t => t.nm_aluno).ToList();
            return aluno;
        }
        public void AlterarAluno(Database.Entyti.tb_aluno aluno)
        {
            Entyti.schooldbEntities o = new Entyti.schooldbEntities();
            Entyti.tb_aluno alterar = o.tb_aluno.First(a => a.id_aluno ==aluno.id_aluno);
            alterar.nm_aluno = aluno.nm_aluno;
            alterar.ds_bairro = aluno.ds_bairro;
            alterar.nr_chamada = aluno.nr_chamada;
            alterar.ds_municipio = aluno.ds_municipio;
            alterar.dt_nascimento = aluno.dt_nascimento;
            alterar.id_turma = aluno.id_turma;
            o.SaveChanges();

        }
        public void RemoverAluno(Database.Entyti.tb_aluno aluno)
        {
            Entyti.schooldbEntities o = new Entyti.schooldbEntities();

            Entyti.tb_aluno remove = o.tb_aluno.First(a => a.id_aluno == aluno.id_aluno);
            o.tb_aluno.Remove(remove);

            o.SaveChanges();
        }
        public List<Database.Entyti.tb_aluno> ConsultarPelaTurma (Database.Entyti.tb_turma turma)

        {
        Database.Entyti.schooldbEntities db = new Database.Entyti.schooldbEntities();
        List<Database.Entyti.tb_aluno> list = db.tb_aluno.Where(t => t.id_turma == turma.id_turma).OrderBy(t => t.nm_aluno).ToList();
        int a = 0; foreach (Database.Entyti.tb_aluno ajuste in list)
        { a++; ajuste.nr_chamada = a; db.SaveChanges(); }
         return list;



        }

    }
}

