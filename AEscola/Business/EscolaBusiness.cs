﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEscola.Business
{
    class EscolaBusiness
    {
        public void Inserir(Database.Entyti.tb_turma turma)
        {
            if (turma.nm_curso == string.Empty || turma.nm_curso == string.Empty)
            {
                throw new ArgumentException("Algum campo não inserido");
            }
            if (turma.qt_max_alunos < 25)
            {
                throw new ArgumentException("Minimo 25 alunos");
            }
            string t = turma.nm_curso;
            string b = t.Trim();

            Database.DatabaseEscola A = new Database.DatabaseEscola();
            A.Inserir(turma);

        }

        public List<Database.Entyti.tb_turma> Listar()
        {
            Database.DatabaseEscola A = new Database.DatabaseEscola();
            List<Database.Entyti.tb_turma> turmas = A.Listar();

            return turmas;
        }

        public List<Database.Entyti.tb_turma> Consultar(int max, string curso)
        {
            if (curso == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();

            List<Database.Entyti.tb_turma> turmas = db.Consultar(curso, max);
            return turmas;
        }

        public List<Database.Entyti.tb_aluno> consutarporAluno (Database.Entyti.tb_turma turma)
        {
            if (turma == null)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            List<Database.Entyti.tb_turma> turmas = db.ConsultarPelaTurma(turma);
            return turmas ;

        }

        public List<Database.Entyti.tb_turma> Consultar2(int max)
        {
            if (max == null)
            {
                throw new ArgumentException("number é obrigatório");
            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();

            List<Database.Entyti.tb_turma> turmas = db.Consultar2(max);
            return turmas;
        }

        public List<Database.Entyti.tb_turma> Consultar3(string curso)
        {
            if (curso == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();

            List<Database.Entyti.tb_turma> turmas = db.Consultar3(curso);
            return turmas;
        }

        public List<Database.Entyti.tb_turma> Consultar4(string turma)
        {
            if (turma == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();

            List<Database.Entyti.tb_turma> turmas = db.Consultar4(turma);
            return turmas;
        }

        public void Remover(Database.Entyti.tb_turma turma)
        {
            if (turma.id_turma == null)
            {
                throw new ArgumentException("Insira um Numero");

            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            db.Remover(turma);

        }

        public void Alterar(Database.Entyti.tb_turma turma)
        {
            if (turma.id_turma == null)
            {
                throw new ArgumentException("Insira um Numero");

            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            db.Alterar(turma);

        }

        public void Remover2(Database.Entyti.tb_turma turma)
        {
            if (turma.nm_turma == null)
            {
                throw new ArgumentException("Insira uma Turma");

            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            db.Remover2(turma);

        }
        public void Remover3(Database.Entyti.tb_turma turma)
        {
            if (turma.nm_turma == null)
            {
                throw new ArgumentException("Insira uma Turma");

            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            db.Remover2(turma);

        }
        public void Alterar2(Database.Entyti.tb_turma turma)
        {
            if (turma.nm_turma == null)
            {
                throw new ArgumentException("Insira um Numero");

            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            db.Alterar2(turma);
        }
        public void Alterar3(Database.Entyti.tb_turma turma)
        {
            if (turma.nm_turma == null)
            {
                throw new ArgumentException("Insira uma Turma");

            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            db.Alterar2(turma);

        }
        public void InserirAluno(Database.Entyti.tb_aluno aluno)
        {
            if (aluno.nm_aluno == string.Empty || aluno.nm_aluno == string.Empty)
            {
                throw new ArgumentException("Algum campo não inserido");
            }
            if (aluno.ds_bairro == string.Empty || aluno.ds_bairro == string.Empty)
            {
                throw new ArgumentException("Algum campo não inserido");
            }
            if (aluno.ds_municipio == string.Empty || aluno.ds_municipio == string.Empty)
            {
                throw new ArgumentException("Algum campo não inserido");
            }
            if (aluno.dt_nascimento == null || aluno.dt_nascimento == null)
            {
                throw new ArgumentException("Algum campo não inserido");
            }
            if (aluno.id_turma == null || aluno.id_turma == null)
            {
                throw new ArgumentException("Algum campo não inserido");
            }
           
            

            Database.DatabaseEscola A = new Database.DatabaseEscola();
            A.InserirAluno(aluno);

        }
        public List<Database.Entyti.tb_aluno> ConsultarAluno()
        {
            Database.DatabaseEscola db = new Database.DatabaseEscola();

            List<Database.Entyti.tb_aluno> aluno = db.ConsultarAluno();
            return aluno;
        }
        public void AlterarAluno(Database.Entyti.tb_aluno aluno)
        {
            if (aluno.id_aluno == null)
            {
                throw new ArgumentException("Insira um Numero");
            
            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            db.AlterarAluno(aluno);
        
        }
        public void RemoverAluno(Database.Entyti.tb_aluno aluno)
        {
            if (aluno.id_aluno == null|| aluno.id_aluno == 0)
            {
                throw new ArgumentException("Insira um Numero");

            }

            Database.DatabaseEscola db = new Database.DatabaseEscola();
            db.RemoverAluno(aluno);

        }
    }
}