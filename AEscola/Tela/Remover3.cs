﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class Remover3 : Form
    {
        public Remover3()
        {
            InitializeComponent();

            this.CarregarCombo();
        }

        private void CarregarCombo()
        {
             Business.EscolaBusiness b = new Business.EscolaBusiness();
            List<Database.Entyti.tb_turma> turmas = b.Listar();

            cbx1.DisplayMember = nameof(Database.Entyti.tb_turma.nm_turma);
            cbx1.DataSource = turmas;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entyti.tb_turma turma = new Database.Entyti.tb_turma();
                turma.nm_turma = cbx1.Text;

                Business.EscolaBusiness A = new Business.EscolaBusiness();
                A.Remover3(turma);

                MessageBox.Show("Removido");
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }

        private void Remover3_Load(object sender, EventArgs e)
        {

        }

        private void cbx1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
