﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class InserirAluno : Form
    {
        public InserirAluno()
        {
            InitializeComponent();
            this.CarregarCombo();
        }

        private void CarregarCombo()
        {
            Business.EscolaBusiness b = new Business.EscolaBusiness();
            List<Database.Entyti.tb_turma> turmas = b.Listar();

            cbxTurma.DisplayMember = nameof(Database.Entyti.tb_turma.nm_turma);
            cbxTurma.DataSource = turmas;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entyti.tb_turma Turmas = cbxTurma.SelectedItem as Database.Entyti.tb_turma;


                int ID = Turmas.id_turma;
                string Turma = cbxTurma.Text;
                string Nome = txtNome.Text;
                string Bairro = txtBairro.Text;
                string Municipio = txtMunicipio.Text;
                int Numero = Convert.ToInt32(nud1.Value);
                DateTime data = dtpNascimento.Value;

                Database.Entyti.tb_aluno A = new Database.Entyti.tb_aluno();

                A.id_turma = ID;
                A.nm_aluno = Nome;
                A.nr_chamada = Numero;
                A.ds_bairro = Bairro;
                A.ds_municipio = Municipio;
                A.dt_nascimento = data;

                Business.EscolaBusiness B = new Business.EscolaBusiness();
                B.InserirAluno(A);       
              

                MessageBox.Show("Inserido");
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbxTurma_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtMunicipio_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBairro_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtpNascimento_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void nud1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
