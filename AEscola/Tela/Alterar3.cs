﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class Alterar3 : Form
    {
        public Alterar3()
        {
            InitializeComponent();

            Business.EscolaBusiness b = new Business.EscolaBusiness();
            List<Database.Entyti.tb_turma> turmas = b.Listar();

            cbx2.DisplayMember = nameof(Database.Entyti.tb_turma.nm_turma);
            cbx2.DataSource = turmas;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entyti.tb_turma A = new Database.Entyti.tb_turma();
                A.nm_curso = cbx1.Text;
                A.nm_turma = cbx2.Text;
                A.qt_max_alunos = Convert.ToInt32(nud2.Value);
                Business.EscolaBusiness B = new Business.EscolaBusiness();
                B.Alterar2(A);

                MessageBox.Show("Alterado com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
