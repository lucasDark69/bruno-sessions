﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class RemoverAluno : Form
    {
        public RemoverAluno()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entyti.tb_aluno aluno = new Database.Entyti.tb_aluno();
                aluno.id_aluno = Convert.ToInt32(nud1.Value);

                Business.EscolaBusiness A = new Business.EscolaBusiness();
                A.RemoverAluno(aluno);

                MessageBox.Show("Removido");
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }

        }
    }
}
