﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class AlterarAluno : Form
    {
        public AlterarAluno()
        {
            InitializeComponent();
            this.CarregarCombo();
        }

        private void CarregarCombo()
        {
            Business.EscolaBusiness b = new Business.EscolaBusiness();
            List<Database.Entyti.tb_turma> turmas = b.Listar();

            cbxTurma.DisplayMember = nameof(Database.Entyti.tb_turma.nm_turma);
            cbxTurma.DataSource = turmas;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entyti.tb_turma Turmas = cbxTurma.SelectedItem as Database.Entyti.tb_turma;
                Database.Entyti.tb_aluno A = new Database.Entyti.tb_aluno();
                A.id_aluno = Convert.ToInt32(nud1.Value);
                A.nm_aluno = txtNome.Text;
                A.id_turma = Turmas.id_turma;
                A.ds_bairro = txtBairro.Text;
                A.ds_municipio = txtMunicipio.Text;
                A.nr_chamada = Convert.ToInt32(numericUpDown1.Value);
                A.dt_nascimento = dtpNascimento.Value;

                Business.EscolaBusiness B = new Business.EscolaBusiness();
                B.AlterarAluno(A);

                MessageBox.Show("Alterado com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
