﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class Remover : Form
    {
        public Remover()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entyti.tb_turma turma = new Database.Entyti.tb_turma();
                turma.id_turma = Convert.ToInt32(nud1.Value);

                Business.EscolaBusiness A = new Business.EscolaBusiness();
                A.Remover(turma);

                MessageBox.Show("Removido");
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void nud1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
