﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class Consultar3 : Form
    {
        public Consultar3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string turma = txt1.Text;


            Business.EscolaBusiness A = new Business.EscolaBusiness();
            List<Database.Entyti.tb_turma> turmas = A.Consultar3(turma);

            dgv1.DataSource = turmas;
        }
    }
}
