﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class Inserir : Form
    {
        public Inserir()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string curso = cbx1.Text;
                string turma = txt1.Text;
                decimal maxA = nud.Value;

                Database.Entyti.tb_turma A = new Database.Entyti.tb_turma();
                A.nm_curso = curso;
                A.nm_turma = turma;
                A.qt_max_alunos = Convert.ToInt32(maxA);

                Business.EscolaBusiness B = new Business.EscolaBusiness();
                B.Inserir(A);

                MessageBox.Show("Inserido");
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
