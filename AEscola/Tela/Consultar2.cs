﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class Consultar2 : Form
    {
        public Consultar2()
        {
            InitializeComponent();
        }

        private void Consultar2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
      
            int max = Convert.ToInt32(nud1.Value);

            Business.EscolaBusiness A = new Business.EscolaBusiness();
            List<Database.Entyti.tb_turma> turmas = A.Consultar2(max);

            dgv1.DataSource = turmas;
        }
    }
}
