﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class MenAlterar : Form
    {
        public MenAlterar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tela.Alterar a = new Tela.Alterar();
            a.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Tela.Alterar2 a = new Tela.Alterar2();
            a.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Tela.Alterar3 a = new Tela.Alterar3();
            a.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Tela.AlterarAluno a = new Tela.AlterarAluno();
            a.ShowDialog();
        }
    }
}
