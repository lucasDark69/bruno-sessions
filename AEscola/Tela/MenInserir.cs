﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class MenInserir : Form
    {
        public MenInserir()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tela.Inserir a = new Inserir();
            a.ShowDialog();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Tela.InserirAluno A = new InserirAluno();
            A.ShowDialog();
        }
    }
}
