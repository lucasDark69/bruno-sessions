﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEscola.Tela
{
    public partial class MenConsultar : Form
    {
        public MenConsultar()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Tela.Consultar4 a = new Tela.Consultar4();
            a.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tela.Consultar a = new Tela.Consultar();
            a.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Tela.Consultar2 a = new Tela.Consultar2();
            a.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Tela.Consultar3 a = new Tela.Consultar3();
            a.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Tela.ConsultarAluno a = new Tela.ConsultarAluno();
            a.ShowDialog();
        }
    }
}
